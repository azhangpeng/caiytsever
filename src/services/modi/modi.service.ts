// Initializes the `modi` service on path `/modi`
import { ServiceAddons } from '@feathersjs/feathers';
import { Application } from '../../declarations';
import { Modi } from './modi.class';
import createModel from '../../models/modi.model';
import hooks from './modi.hooks';

// Add this service to the service type index
declare module '../../declarations' {
  interface ServiceTypes {
    'modi': Modi & ServiceAddons<any>;
  }
}

export default function (app: Application): void {
  const options = {
    Model: createModel(app),
    paginate: app.get('paginate')
  };

  // Initialize our service with any options it requires
  app.use('/modi', new Modi(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service('modi');

  service.hooks(hooks);
}
