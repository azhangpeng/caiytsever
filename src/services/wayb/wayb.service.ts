// Initializes the `wayb` service on path `/wayb`
import { ServiceAddons } from '@feathersjs/feathers';
import { Application } from '../../declarations';
import { Wayb } from './wayb.class';
import createModel from '../../models/wayb.model';
import hooks from './wayb.hooks';

// Add this service to the service type index
declare module '../../declarations' {
  interface ServiceTypes {
    'wayb': Wayb & ServiceAddons<any>;
  }
}

export default function (app: Application): void {
  const options = {
    Model: createModel(app),
    paginate: app.get('paginate')
  };

  // Initialize our service with any options it requires
  app.use('/wayb', new Wayb(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service('wayb');

  service.hooks(hooks);
}
