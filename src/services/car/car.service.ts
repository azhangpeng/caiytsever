// Initializes the `car` service on path `/car`
import { ServiceAddons } from '@feathersjs/feathers';
import { Application } from '../../declarations';
import { Car } from './car.class';
import createModel from '../../models/car.model';
import hooks from './car.hooks';

// Add this service to the service type index
declare module '../../declarations' {
  interface ServiceTypes {
    'car': Car & ServiceAddons<any>;
  }
}

export default function (app: Application): void {
  const options = {
    Model: createModel(app),
    paginate: app.get('paginate')
  };

  // Initialize our service with any options it requires
  app.use('/car', new Car(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service('car');

  service.hooks(hooks);
}
