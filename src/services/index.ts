import { Application } from '../declarations';
import users from './users/users.service';
import oders from './oders/oders.service';
import car from './car/car.service';
import drivers from './drivers/drivers.service';
import way from './way/way.service';
import serve from './serve/serve.service';
import out from './out/out.service';
import wayb from './wayb/wayb.service';
import modi from './modi/modi.service';
import yundangz from './yundangz/yundangz.service';
import waybill from './waybill/waybill.service';
import vehicle from './vehicle/vehicle.service';
// Don't remove this comment. It's needed to format import lines nicely.

export default function (app: Application): void {
  app.configure(users);
  app.configure(oders);
  app.configure(car);
  app.configure(drivers);
  app.configure(way);
  app.configure(serve);
  app.configure(out);
  app.configure(wayb);
  app.configure(modi);
  app.configure(yundangz);
  app.configure(waybill);
  app.configure(vehicle);
}
