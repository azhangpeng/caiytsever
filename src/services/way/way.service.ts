// Initializes the `way` service on path `/way`
import { ServiceAddons } from '@feathersjs/feathers';
import { Application } from '../../declarations';
import { Way } from './way.class';
import createModel from '../../models/way.model';
import hooks from './way.hooks';

// Add this service to the service type index
declare module '../../declarations' {
  interface ServiceTypes {
    'way': Way & ServiceAddons<any>;
  }
}

export default function (app: Application): void {
  const options = {
    Model: createModel(app),
    paginate: app.get('paginate')
  };

  // Initialize our service with any options it requires
  app.use('/way', new Way(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service('way');

  service.hooks(hooks);
}
