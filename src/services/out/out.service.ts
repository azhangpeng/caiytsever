// Initializes the `out` service on path `/out`
import { ServiceAddons } from '@feathersjs/feathers';
import { Application } from '../../declarations';
import { Out } from './out.class';
import createModel from '../../models/out.model';
import hooks from './out.hooks';

// Add this service to the service type index
declare module '../../declarations' {
  interface ServiceTypes {
    'out': Out & ServiceAddons<any>;
  }
}

export default function (app: Application): void {
  const options = {
    Model: createModel(app),
    paginate: app.get('paginate')
  };

  // Initialize our service with any options it requires
  app.use('/out', new Out(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service('out');

  service.hooks(hooks);
}
