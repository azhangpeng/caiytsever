// Initializes the `waybill` service on path `/waybill`
import { ServiceAddons } from '@feathersjs/feathers';
import { Application } from '../../declarations';
import { Waybill } from './waybill.class';
import createModel from '../../models/waybill.model';
import hooks from './waybill.hooks';

// Add this service to the service type index
declare module '../../declarations' {
  interface ServiceTypes {
    'waybill': Waybill & ServiceAddons<any>;
  }
}

export default function (app: Application): void {
  const options = {
    Model: createModel(app),
    paginate: app.get('paginate')
  };

  // Initialize our service with any options it requires
  app.use('/waybill', new Waybill(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service('waybill');

  service.hooks(hooks);
}
