// Initializes the `yundangz` service on path `/yundangz`
import { ServiceAddons } from '@feathersjs/feathers';
import { Application } from '../../declarations';
import { Yundangz } from './yundangz.class';
import createModel from '../../models/yundangz.model';
import hooks from './yundangz.hooks';

// Add this service to the service type index
declare module '../../declarations' {
  interface ServiceTypes {
    'yundangz': Yundangz & ServiceAddons<any>;
  }
}

export default function (app: Application): void {
  const options = {
    Model: createModel(app),
    paginate: app.get('paginate')
  };

  // Initialize our service with any options it requires
  app.use('/yundangz', new Yundangz(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service('yundangz');

  service.hooks(hooks);
}
