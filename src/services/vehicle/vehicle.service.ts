// Initializes the `vehicle` service on path `/vehicle`
import { ServiceAddons } from '@feathersjs/feathers';
import { Application } from '../../declarations';
import { Vehicle } from './vehicle.class';
import createModel from '../../models/vehicle.model';
import hooks from './vehicle.hooks';

// Add this service to the service type index
declare module '../../declarations' {
  interface ServiceTypes {
    'vehicle': Vehicle & ServiceAddons<any>;
  }
}

export default function (app: Application): void {
  const options = {
    Model: createModel(app),
    paginate: app.get('paginate')
  };

  // Initialize our service with any options it requires
  app.use('/vehicle', new Vehicle(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service('vehicle');

  service.hooks(hooks);
}
