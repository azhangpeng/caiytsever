// Initializes the `oders` service on path `/oders`
import { ServiceAddons } from '@feathersjs/feathers';
import { Application } from '../../declarations';
import { Oders } from './oders.class';
import createModel from '../../models/oders.model';
import hooks from './oders.hooks';

// Add this service to the service type index
declare module '../../declarations' {
  interface ServiceTypes {
    'oders': Oders & ServiceAddons<any>;
  }
}

export default function (app: Application): void {
  const options = {
    Model: createModel(app),
    paginate: app.get('paginate')
  };

  // Initialize our service with any options it requires
  app.use('/oders', new Oders(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service('oders');

  service.hooks(hooks);
}
