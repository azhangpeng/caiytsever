// See https://sequelize.org/master/manual/model-basics.html
// for more of what you can do here.
import { Sequelize, DataTypes, Model } from 'sequelize';
import { Application } from '../declarations';
import { HookReturn } from 'sequelize/types/hooks';

export default function (app: Application): typeof Model {
  const sequelizeClient: Sequelize = app.get('sequelizeClient');
  const waybill = sequelizeClient.define('waybill', {
    suoshuzj: {
      type: DataTypes.STRING,
      allowNull: false
    },
    waybillid: {
      type: DataTypes.STRING,
      allowNull: false
    },
    shoukuan: {
      type: DataTypes.STRING,
      allowNull: false
    },
    tel: {
      type: DataTypes.STRING,
      allowNull: false
    },
    card : {
      type: DataTypes.STRING,
      allowNull: false
    },
    cardid: {
      type: DataTypes.STRING,
      allowNull: false
    },
    Way: {
      type: DataTypes.STRING,
      allowNull: false
    },
    source: {
      type: DataTypes.STRING,
      allowNull: false
    },
    onTime: {
      type: DataTypes.STRING,
      allowNull: false
    },
    wait: {
      type: DataTypes.STRING,
      allowNull: false
    },
    cost: {
      type: DataTypes.STRING,
      allowNull: false
    },
    reckoner: {
      type: DataTypes.STRING,
      allowNull: false
    },
    tip: {
      type: DataTypes.STRING,
      allowNull: false
    },
  }, {
    hooks: {
      beforeCount(options: any): HookReturn {
        options.raw = true;
      }
    }
  });

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  (waybill as any).associate = function (models: any): void {
    // Define associations here
    // See https://sequelize.org/master/manual/assocs.html
  };

  return waybill;
}
