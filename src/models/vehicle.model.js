"use strict";
exports.__esModule = true;
// See https://sequelize.org/master/manual/model-basics.html
// for more of what you can do here.
var sequelize_1 = require("sequelize");
function default_1(app) {
    var sequelizeClient = app.get('sequelizeClient');
    var vehicle = sequelizeClient.define('vehicle', {
        cz: {
            type: sequelize_1.DataTypes.STRING,
            allowNull: false
        },
        zuzhi: {
            type: sequelize_1.DataTypes.STRING,
            allowNull: false
        },
        vehicleid: {
            type: sequelize_1.DataTypes.STRING,
            allowNull: false
        },
        status: {
            type: sequelize_1.DataTypes.STRING,
            allowNull: false
        },
        color: {
            type: sequelize_1.DataTypes.STRING,
            allowNull: false
        },
        type: {
            type: sequelize_1.DataTypes.STRING,
            allowNull: false
        },
        long: {
            type: sequelize_1.DataTypes.STRING,
            allowNull: false
        },
        load: {
            type: sequelize_1.DataTypes.STRING,
            allowNull: false
        },
        photo: {
            type: sequelize_1.DataTypes.STRING,
            allowNull: false
        }
    }, {
        hooks: {
            beforeCount: function (options) {
                options.raw = true;
            }
        }
    });
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    vehicle.associate = function (models) {
        // Define associations here
        // See https://sequelize.org/master/manual/assocs.html
    };
    return vehicle;
}
exports["default"] = default_1;
