// See https://sequelize.org/master/manual/model-basics.html
// for more of what you can do here.
import { Sequelize, DataTypes, Model } from 'sequelize';
import { Application } from '../declarations';
import { HookReturn } from 'sequelize/types/hooks';

export default function (app: Application): typeof Model {
  const sequelizeClient: Sequelize = app.get('sequelizeClient');
  const out = sequelizeClient.define('out', {
    fare: {
      type: DataTypes.STRING,
      allowNull: false
    },
    transportation: {
      type: DataTypes.STRING,
      allowNull: false
    },
    handling: {
      type: DataTypes.STRING,
      allowNull: false
    },
    oil: {
      type: DataTypes.STRING,
      allowNull: false
    },
    oilCard: {
      type: DataTypes.STRING,
      allowNull: false
    },
    coilCashash: {
      type: DataTypes.STRING,
      allowNull: false
    },
    urea: {
      type: DataTypes.STRING,
      allowNull: false
    },
    bridge: {
      type: DataTypes.STRING,
      allowNull: false
    },
    else: {
      type: DataTypes.STRING,
      allowNull: false
    },
    sum: {
      type: DataTypes.STRING,
      allowNull: false
    },
    notes: {
      type: DataTypes.STRING,
      allowNull: false
    },
  }, {
    hooks: {
      beforeCount(options: any): HookReturn {
        options.raw = true;
      }
    }
  });

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  (out as any).associate = function (models: any): void {
    // Define associations here
    // See https://sequelize.org/master/manual/assocs.html
  };

  return out;
}
