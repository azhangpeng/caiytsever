// See https://sequelize.org/master/manual/model-basics.html
// for more of what you can do here.
import { Sequelize, DataTypes, Model } from 'sequelize';
import { Application } from '../declarations';
import { HookReturn } from 'sequelize/types/hooks';

export default function (app: Application): typeof Model {
  const sequelizeClient: Sequelize = app.get('sequelizeClient');
  const wayb = sequelizeClient.define('wayb', {
    waybill: {
      type: DataTypes.STRING,
      allowNull: false
    },
    waybillstatus: {
      type: DataTypes.STRING,
      allowNull: false
    },
    create: {
      type: DataTypes.STRING,
      allowNull: false
    },
    system : {
      type: DataTypes.STRING,
      allowNull: false
    },
    name: {
      type: DataTypes.STRING,
      allowNull: false
    },
    twoWay: {
      type: DataTypes.STRING,
      allowNull: false
    },
    source: {
      type: DataTypes.STRING,
      allowNull: false
    },
    onTime: {
      type: DataTypes.STRING,
      allowNull: false
    },
    wait: {
      type: DataTypes.STRING,
      allowNull: false
    },
    cost: {
      type: DataTypes.STRING,
      allowNull: false
    },
    reckoner: {
      type: DataTypes.STRING,
      allowNull: false
    },
    tip: {
      type: DataTypes.STRING,
      allowNull: false
    },
  }, {
    hooks: {
      beforeCount(options: any): HookReturn {
        options.raw = true;
      }
    }
  });

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  (wayb as any).associate = function (models: any): void {
    // Define associations here
    // See https://sequelize.org/master/manual/assocs.html
  };

  return wayb;
}
