// See https://sequelize.org/master/manual/model-basics.html
// for more of what you can do here.
import { Sequelize, DataTypes, Model } from 'sequelize';
import { Application } from '../declarations';
import { HookReturn } from 'sequelize/types/hooks';

export default function (app: Application): typeof Model {
  const sequelizeClient: Sequelize = app.get('sequelizeClient');
  const drivers = sequelizeClient.define('drivers', {
    sijiZu: {
      type: DataTypes.STRING,
      allowNull: false
    },
    sijiYong: {
      type: DataTypes.STRING,
      allowNull: false
    },
    siji: {
      type: DataTypes.STRING,
      allowNull: false
    },
    sijiTel: {
      type: DataTypes.STRING,
      allowNull: false
    },
    inPhoto: {
      type: DataTypes.STRING,
      allowNull: false
    },
    sijiTpe: {
      type: DataTypes.STRING,
      allowNull: false
    }
  }, {
    hooks: {
      beforeCount(options: any): HookReturn {
        options.raw = true;
      }
    }
  });

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  (drivers as any).associate = function (models: any): void {
    // Define associations here
    // See https://sequelize.org/master/manual/assocs.html
  };

  return drivers;
}
