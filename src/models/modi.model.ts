// See https://sequelize.org/master/manual/model-basics.html
// for more of what you can do here.
import { Sequelize, DataTypes, Model } from 'sequelize';
import { Application } from '../declarations';
import { HookReturn } from 'sequelize/types/hooks';

export default function (app: Application): typeof Model {
  const sequelizeClient: Sequelize = app.get('sequelizeClient');
  const modi = sequelizeClient.define(
    'modi',
    {
      applicant: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      modifyR: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      unusual: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      unState: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      unType: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      unNum: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      unDescribe: {
        type: DataTypes.STRING,
        allowNull: true,
      },
      djNetwork: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      djRen: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      djTime: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      phone: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      organization: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      chuli: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      chuliDian: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      chuliRen: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      chiliShi: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      fukuan: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      shoukuan: {
        type: DataTypes.STRING,
        allowNull: false,
      },
    },
    {
      hooks: {
        beforeCount(options: any): HookReturn {
          options.raw = true;
        },
      },
    }
  );

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  (modi as any).associate = function (models: any): void {
    // Define associations here
    // See https://sequelize.org/master/manual/assocs.html
  };

  return modi;
}
