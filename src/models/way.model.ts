// See https://sequelize.org/master/manual/model-basics.html
// for more of what you can do here.
import { Sequelize, DataTypes, Model } from 'sequelize';
import { Application } from '../declarations';
import { HookReturn } from 'sequelize/types/hooks';

export default function (app: Application): typeof Model {
  const sequelizeClient: Sequelize = app.get('sequelizeClient');
  const way = sequelizeClient.define('way', {
    project: {
      type: DataTypes.STRING,
      allowNull: false
    },
    customerName: {
      type: DataTypes.STRING,
      allowNull: true
    },
    start: {
      type: DataTypes.STRING,
      allowNull: false
    },
    end: {
      type: DataTypes.STRING,
      allowNull: false
    },
    sender: {
      type: DataTypes.STRING,
      allowNull: true
    },
    senderTel: {
      type: DataTypes.STRING,
      allowNull: true
    },
    recipient: {
      type: DataTypes.STRING,
      allowNull: true
    },
    recipientTel: {
      type: DataTypes.STRING,
      allowNull: true
    },
    expectOut: {
      type: DataTypes.STRING,
      allowNull: true
    },
    expectIn: {
      type: DataTypes.STRING,
      allowNull: true
    },
    realityOut: {
      type: DataTypes.STRING,
      allowNull: true
    },
    realityIn: {
      type: DataTypes.STRING,
      allowNull: true
    },
    milename: {
      type: DataTypes.STRING,
      allowNull: false
    },
    mileage: {
      type: DataTypes.STRING,
      allowNull: true
    },
    ageing: {
      type: DataTypes.STRING,
      allowNull: true
    },
    needType: {
      type: DataTypes.STRING,
      allowNull: true
    },
    needLong: {
      type: DataTypes.STRING,
      allowNull: true
    },
    good: {
      type: DataTypes.STRING,
      allowNull: true
    },
    RMB: {
      type: DataTypes.STRING,
      allowNull: true
    },
    piece: {
      type: DataTypes.STRING,
      allowNull: true
    },
    weight: {
      type: DataTypes.STRING,
      allowNull: true
    },
    size: {
      type: DataTypes.STRING,
      allowNull: true
    },
    income: {
      type: DataTypes.STRING,
      allowNull: true
    },
    freight: {
      type: DataTypes.STRING,
      allowNull: true
    },
    locationFee: {
      type: DataTypes.STRING,
      allowNull: true
    },
    value: {
      type: DataTypes.STRING,
      allowNull: true
    },
    charge: {
      type: DataTypes.STRING,
      allowNull: true
    },
    method: {
      type: DataTypes.STRING,
      allowNull: true
    },
    form: {
      type: DataTypes.STRING,
      allowNull: true
    },
    card: {
      type: DataTypes.STRING,
      allowNull: true
    },
    cash: {
      type: DataTypes.STRING,
      allowNull: true
    },
    collection: {
      type: DataTypes.STRING,
      allowNull: true
    },
    back: {
      type: DataTypes.STRING,
      allowNull: true
    },
    settlement: {
      type: DataTypes.STRING,
      allowNull: true
    },
  }, {
    hooks: {
      beforeCount(options: any): HookReturn {
        options.raw = true;
      }
    }
  });

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  (way as any).associate = function (models: any): void {
    // Define associations here
    // See https://sequelize.org/master/manual/assocs.html
  };

  return way;
}
