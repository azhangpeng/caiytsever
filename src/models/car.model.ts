// See https://sequelize.org/master/manual/model-basics.html
// for more of what you can do here.
import { Sequelize, DataTypes, Model } from "sequelize";
import { Application } from "../declarations";
import { HookReturn } from "sequelize/types/hooks";

export default function (app: Application): typeof Model {
  const sequelizeClient: Sequelize = app.get("sequelizeClient");
  const car = sequelizeClient.define(
    "car",
    {
      cheZu: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      cheYong: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      carNum: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      carStatus: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      carColor: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      carLike: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      cheChang: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      zaiZhong: {
        type: DataTypes.STRING,
        allowNull: false,
      },
    },
    {
      hooks: {
        beforeCount(options: any): HookReturn {
          options.raw = true;
        },
      },
    }
  );

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  (car as any).associate = function (models: any): void {
    // Define associations here
    // See https://sequelize.org/master/manual/assocs.html
  };

  return car;
}
