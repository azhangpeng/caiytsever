// See https://sequelize.org/master/manual/model-basics.html
// for more of what you can do here.
import { Sequelize, DataTypes, Model } from 'sequelize';
import { Application } from '../declarations';
import { HookReturn } from 'sequelize/types/hooks';

export default function (app: Application): typeof Model {
  const sequelizeClient: Sequelize = app.get('sequelizeClient');
  const oders = sequelizeClient.define('oders', {
    orderNo: {
      type: DataTypes.STRING,
      allowNull: false
    },
    billTime:{
      type:DataTypes.TIME,
      allowNull: false
    },
    customer : {
      type: DataTypes.STRING,
      allowNull: false
    },
    fa: {
      type: DataTypes.STRING,
      allowNull: false
    },
    dao:{
      type: DataTypes.STRING,
      allowNull: false
    },
    network:{
      type: DataTypes.STRING,
      allowNull: false
    },
    shipper:{
      type: DataTypes.STRING,
      allowNull: false
    },
    faContact:{
      type: DataTypes.STRING,
      allowNull: false
    },
    faPhone:{
      type: DataTypes.STRING,
      allowNull: false
    },
    faAddress:{
      type: DataTypes.STRING,
      allowNull: false
    },
    fabei:{
      type: DataTypes.STRING,
      allowNull: false
    },
    to​:{
      type: DataTypes.STRING,
      allowNull: false
    },
    shouContact:{
      type: DataTypes.STRING,
      allowNull: false
    },
    shouPhone:{
      type: DataTypes.STRING,
      allowNull: false
    },
    shouAddress:{
      type: DataTypes.STRING,
      allowNull: false
    },
    shouMark:{
      type: DataTypes.STRING,
      allowNull: false
    },
    goods:{
      type: DataTypes.STRING,
      allowNull: false
    },
    pieces:{
      type: DataTypes.STRING,
      allowNull: false
    },
    weight:{
      type: DataTypes.STRING,
      allowNull: false
    },
    total:{
      type: DataTypes.STRING,
      allowNull: false
    },
    freight:{
      type: DataTypes.STRING,
      allowNull: false
    },
    songFee:{
      type: DataTypes.STRING,
      allowNull: false
    },
    tiFee:{
      type: DataTypes.STRING,
      allowNull: false
    },
    xieFee:{
      type: DataTypes.STRING,
      allowNull: false
    },​
    payment:{
      type: DataTypes.STRING,
      allowNull: false
    },
    cash:{
      type: DataTypes.STRING,
      allowNull: false
    },
    collect:{
      type: DataTypes.STRING,
      allowNull: false
    },
    monthly:{
      type: DataTypes.STRING,
      allowNull: false
    },
    back:{
      type: DataTypes.STRING,
      allowNull: false
    },
    operator:{
      type: DataTypes.STRING,
      allowNull: false
    },
    songMethod:{
      type: DataTypes.STRING,
      allowNull: false
    },
    receipt:{
      type: DataTypes.STRING,
      allowNull: false
    },
    orderRemarks:{
      type: DataTypes.STRING,
      allowNull: false
    },
    organization:{
      type: DataTypes.STRING,
      allowNull: false
    },
    orderStatus:{
      type: DataTypes.STRING,
      allowNull: false
    },
    auditStatus:{
      type: DataTypes.STRING,
      allowNull: false
    },
    changeType:{
      type: DataTypes.STRING,
      allowNull: false
    },
    changeReason:{
      type: DataTypes.STRING,
      allowNull: false
    },
    change:{
      type: DataTypes.STRING,
      allowNull: false
    },
    changeStatus:{
      type: DataTypes.STRING,
      allowNull: false
    }

  }, {
    hooks: {
      beforeCount(options: any): HookReturn {
        options.raw = true;
      }
    }
  });

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  (oders as any).associate = function (models: any): void {
    // Define associations here
    // See https://sequelize.org/master/manual/assocs.html
  };

  return oders;
}
