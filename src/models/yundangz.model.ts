// See https://sequelize.org/master/manual/model-basics.html
// for more of what you can do here.
import { Sequelize, DataTypes, Model } from 'sequelize';
import { Application } from '../declarations';
import { HookReturn } from 'sequelize/types/hooks';

export default function (app: Application): typeof Model {
  const sequelizeClient: Sequelize = app.get('sequelizeClient');
  const yundangz = sequelizeClient.define('yundangz', {
    yundgz: {
      type: DataTypes.STRING,
      allowNull: false
    },
    kehugz: {
      type: DataTypes.STRING,
      allowNull: false
    },
    chexiangz: {
      type: DataTypes.STRING,
      allowNull: false
    },
    yundnagz: {
      type: DataTypes.STRING,
      allowNull: false
    },
    chepaigz: {
      type: DataTypes.STRING,
      allowNull: false
    },
    fagz: {
      type: DataTypes.STRING,
      allowNull: false
    },
    daogz: {
      type: DataTypes.STRING,
      allowNull: false
    },
    chexiangza: {
      type: DataTypes.STRING,
      allowNull: false
    }, 
    sijigz: {
      type: DataTypes.STRING,
      allowNull: false
    }, 
    zhujiagz: {
      type: DataTypes.STRING,
      allowNull: false
    },
  }, {
    hooks: {
      beforeCount(options: any): HookReturn {
        options.raw = true;
      }
    }
  });

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  (yundangz as any).associate = function (models: any): void {
    // Define associations here
    // See https://sequelize.org/master/manual/assocs.html
  };

  return yundangz;
}
