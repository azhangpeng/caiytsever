"use strict";
exports.__esModule = true;
// See https://sequelize.org/master/manual/model-basics.html
// for more of what you can do here.
var sequelize_1 = require("sequelize");
function default_1(app) {
    var sequelizeClient = app.get('sequelizeClient');
    var waybill = sequelizeClient.define('waybill', {
        suoshuzj: {
            type: sequelize_1.DataTypes.STRING,
            allowNull: false
        },
        waybillid: {
            type: sequelize_1.DataTypes.STRING,
            allowNull: false
        },
        shoukuan: {
            type: sequelize_1.DataTypes.STRING,
            allowNull: false
        },
        tel: {
            type: sequelize_1.DataTypes.STRING,
            allowNull: false
        },
        card: {
            type: sequelize_1.DataTypes.STRING,
            allowNull: false
        },
        cardid: {
            type: sequelize_1.DataTypes.STRING,
            allowNull: false
        },
        Way: {
            type: sequelize_1.DataTypes.STRING,
            allowNull: false
        },
        source: {
            type: sequelize_1.DataTypes.STRING,
            allowNull: false
        },
        onTime: {
            type: sequelize_1.DataTypes.STRING,
            allowNull: false
        },
        wait: {
            type: sequelize_1.DataTypes.STRING,
            allowNull: false
        },
        cost: {
            type: sequelize_1.DataTypes.STRING,
            allowNull: false
        },
        reckoner: {
            type: sequelize_1.DataTypes.STRING,
            allowNull: false
        },
        tip: {
            type: sequelize_1.DataTypes.STRING,
            allowNull: false
        }
    }, {
        hooks: {
            beforeCount: function (options) {
                options.raw = true;
            }
        }
    });
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    waybill.associate = function (models) {
        // Define associations here
        // See https://sequelize.org/master/manual/assocs.html
    };
    return waybill;
}
exports["default"] = default_1;
