import assert from 'assert';
import app from '../../src/app';

describe('\'yundangz\' service', () => {
  it('registered the service', () => {
    const service = app.service('yundangz');

    assert.ok(service, 'Registered the service');
  });
});
