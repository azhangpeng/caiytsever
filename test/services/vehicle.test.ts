import assert from 'assert';
import app from '../../src/app';

describe('\'vehicle\' service', () => {
  it('registered the service', () => {
    const service = app.service('vehicle');

    assert.ok(service, 'Registered the service');
  });
});
