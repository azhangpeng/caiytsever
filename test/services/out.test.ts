import assert from 'assert';
import app from '../../src/app';

describe('\'out\' service', () => {
  it('registered the service', () => {
    const service = app.service('out');

    assert.ok(service, 'Registered the service');
  });
});
