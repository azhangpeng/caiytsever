import assert from 'assert';
import app from '../../src/app';

describe('\'car\' service', () => {
  it('registered the service', () => {
    const service = app.service('car');

    assert.ok(service, 'Registered the service');
  });
});
