import assert from 'assert';
import app from '../../src/app';

describe('\'serve\' service', () => {
  it('registered the service', () => {
    const service = app.service('serve');

    assert.ok(service, 'Registered the service');
  });
});
