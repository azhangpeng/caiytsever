import assert from 'assert';
import app from '../../src/app';

describe('\'waybill\' service', () => {
  it('registered the service', () => {
    const service = app.service('waybill');

    assert.ok(service, 'Registered the service');
  });
});
