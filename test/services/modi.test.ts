import assert from 'assert';
import app from '../../src/app';

describe('\'modi\' service', () => {
  it('registered the service', () => {
    const service = app.service('modi');

    assert.ok(service, 'Registered the service');
  });
});
