import assert from 'assert';
import app from '../../src/app';

describe('\'way\' service', () => {
  it('registered the service', () => {
    const service = app.service('way');

    assert.ok(service, 'Registered the service');
  });
});
