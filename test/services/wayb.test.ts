import assert from 'assert';
import app from '../../src/app';

describe('\'wayb\' service', () => {
  it('registered the service', () => {
    const service = app.service('wayb');

    assert.ok(service, 'Registered the service');
  });
});
