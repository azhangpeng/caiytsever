import assert from 'assert';
import app from '../../src/app';

describe('\'oders\' service', () => {
  it('registered the service', () => {
    const service = app.service('oders');

    assert.ok(service, 'Registered the service');
  });
});
